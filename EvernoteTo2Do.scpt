FasdUAS 1.101.10   ��   ��    k             l     ��  ��    ; 5-----------------------------------------------------     � 	 	 j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
  
 l     ��  ��    = 7--------         Evernote To 2Do             ----------     �   n - - - - - - - -                   E v e r n o t e   T o   2 D o                           - - - - - - - - - -      l     ��  ��    ; 5-----------------------------------------------------     �   j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -      l     ��  ��    1 + Author: James Gibbard (jgibbard@gmail.com)     �   V   A u t h o r :   J a m e s   G i b b a r d   ( j g i b b a r d @ g m a i l . c o m )      l     ��  ��      Version: 1.0.2     �      V e r s i o n :   1 . 0 . 2      l     ��������  ��  ��       !   l     �� " #��   "   Last Updated: 06/02/2014    # � $ $ 2   L a s t   U p d a t e d :   0 6 / 0 2 / 2 0 1 4 !  % & % l     ��������  ��  ��   &  ' ( ' l     �� ) *��   )   Description:    * � + +    D e s c r i p t i o n : (  , - , l     �� . /��   . 3 - An AppleScript to create tasks (in 2Do) from    / � 0 0 Z   A n   A p p l e S c r i p t   t o   c r e a t e   t a s k s   ( i n   2 D o )   f r o m -  1 2 1 l     �� 3 4��   3 2 , Evernote notes. It can be used as a script     4 � 5 5 X   E v e r n o t e   n o t e s .   I t   c a n   b e   u s e d   a s   a   s c r i p t   2  6 7 6 l     �� 8 9��   8 "  called from a shortcut key.    9 � : : 8   c a l l e d   f r o m   a   s h o r t c u t   k e y . 7  ; < ; l     ��������  ��  ��   <  = > = l     �� ? @��   ?   Dependent Apps:    @ � A A     D e p e n d e n t   A p p s : >  B C B l     �� D E��   D 1 + - Evernote (http://evernote.com/download/)    E � F F V   -   E v e r n o t e   ( h t t p : / / e v e r n o t e . c o m / d o w n l o a d / ) C  G H G l     �� I J��   I ) # - 2Do (http://www.2doapp.com/mac/)    J � K K F   -   2 D o   ( h t t p : / / w w w . 2 d o a p p . c o m / m a c / ) H  L M L l     ��������  ��  ��   M  N O N l     �� P Q��   P ; 5-----------------------------------------------------    Q � R R j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - O  S T S l     �� U V��   U 8 2           PROPERTIES TO BE AJUSTED             --    V � W W d                       P R O P E R T I E S   T O   B E   A J U S T E D                           - - T  X Y X l     �� Z [��   Z ; 5-----------------------------------------------------    [ � \ \ j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - Y  ] ^ ] l     ��������  ��  ��   ^  _ ` _ l     �� a b��   a   Property: defaultTag    b � c c *   P r o p e r t y :   d e f a u l t T a g `  d e d l     �� f g��   f / ) Sets the default tag that is assinged to    g � h h R   S e t s   t h e   d e f a u l t   t a g   t h a t   i s   a s s i n g e d   t o e  i j i l     �� k l��   k - ' every task that is created from a note    l � m m N   e v e r y   t a s k   t h a t   i s   c r e a t e d   f r o m   a   n o t e j  n o n j     �� p�� 0 
defaulttag 
defaultTag p m      q q � r r  E v e r n o t e o  s t s l     ��������  ��  ��   t  u v u l     �� w x��   w   Property: userTags    x � y y &   P r o p e r t y :   u s e r T a g s v  z { z l     �� | }��   | 4 . Allows the ability to specify if user defined    } � ~ ~ \   A l l o w s   t h e   a b i l i t y   t o   s p e c i f y   i f   u s e r   d e f i n e d {   �  l     �� � ���   � . ( tags should be turned ON (1) or OFF (0)    � � � � P   t a g s   s h o u l d   b e   t u r n e d   O N   ( 1 )   o r   O F F   ( 0 ) �  � � � j    �� ��� 0 usertags userTags � m    ����  �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �   Property: noteTags    � � � � &   P r o p e r t y :   n o t e T a g s �  � � � l     �� � ���   � 4 . Allows the ability to specify if tags defined    � � � � \   A l l o w s   t h e   a b i l i t y   t o   s p e c i f y   i f   t a g s   d e f i n e d �  � � � l     �� � ���   � 2 , on a note should be imported (1) or not (0)    � � � � X   o n   a   n o t e   s h o u l d   b e   i m p o r t e d   ( 1 )   o r   n o t   ( 0 ) �  � � � j    �� ��� 0 notetags noteTags � m    ����  �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �   Property: tagList    � � � � $   P r o p e r t y :   t a g L i s t �  � � � l     �� � ���   � 1 + Sets out the userTags that can be selected    � � � � V   S e t s   o u t   t h e   u s e r T a g s   t h a t   c a n   b e   s e l e c t e d �  � � � l     �� � ���   � ( " when creating a task from a note     � � � � D   w h e n   c r e a t i n g   a   t a s k   f r o m   a   n o t e   �  � � � j   	 �� ��� 0 taglist tagList � J   	  � �  � � � m   	 
 � � � � �  C r e a t e   T a s k s �  � � � m   
  � � � � �  F o r w a r d �  � � � m     � � � � �  R e v i e w �  ��� � m     � � � � �  A c t i o n��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � ; 5-----------------------------------------------------    � � � � j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - �  � � � l     �� � ���   � 0 *  GLOBAL VARIABLES (NOT TO BE AJUSTED)  --    � � � � T     G L O B A L   V A R I A B L E S   ( N O T   T O   B E   A J U S T E D )     - - �  � � � l     �� � ���   � ; 5-----------------------------------------------------    � � � � j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � %  Global Variable: theCustomTags    � � � � >   G l o b a l   V a r i a b l e :   t h e C u s t o m T a g s �  � � � l     �� � ���   � . ( Sets the var for holding all the custom    � � � � P   S e t s   t h e   v a r   f o r   h o l d i n g   a l l   t h e   c u s t o m �  � � � l     �� � ���   � ) # tags to be blank on initialisation    � � � � F   t a g s   t o   b e   b l a n k   o n   i n i t i a l i s a t i o n �  � � � l     ����� � r      � � � m      � � � � �   � o      ���� 0 thecustomtags theCustomTags��  ��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � "  Global Variable: theNoteURL    � � � � 8   G l o b a l   V a r i a b l e :   t h e N o t e U R L �  � � � l     �� � ���   � , & Sets the var for holding the note URL    � � � � L   S e t s   t h e   v a r   f o r   h o l d i n g   t h e   n o t e   U R L �  � � � l     �� � ���   � + % be 'missing value' on initialisation    � � � � J   b e   ' m i s s i n g   v a l u e '   o n   i n i t i a l i s a t i o n �  � � � l    ����� � r     � � � m    ��
�� 
msng � o      ���� 0 
thenoteurl 
theNoteURL��  ��   �    l     ��������  ��  ��    l     ����   ; 5-----------------------------------------------------    � j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��	
��  	 0 *  MAIN SCRIPT (EXECUTION STARTS HERE)   --   
 � T     M A I N   S C R I P T   ( E X E C U T I O N   S T A R T S   H E R E )       - -  l     ����   ; 5-----------------------------------------------------    � j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  l     ��������  ��  ��    l     ����   L F Connect to Evernote (need to use long name, as short name has issues)    � �   C o n n e c t   t o   E v e r n o t e   ( n e e d   t o   u s e   l o n g   n a m e ,   a s   s h o r t   n a m e   h a s   i s s u e s )  l  ���� O    k     Q    > !"  k    ,## $%$ l   ��&'��  & ( " Get the selected note in Evernote   ' �(( D   G e t   t h e   s e l e c t e d   n o t e   i n   E v e r n o t e% )*) r    +,+ 1    ��
�� 
EV15, o      ���� 0 selected_note  * -.- l   ��������  ��  ��  . /0/ l   ��12��  1 0 * Get the note title from the selected note   2 �33 T   G e t   t h e   n o t e   t i t l e   f r o m   t h e   s e l e c t e d   n o t e0 454 r    #676 c    !898 l   :����: n    ;<; 1    ��
�� 
EVet< n    =>= 4    ��?
�� 
cobj? m    ���� > o    ���� 0 selected_note  ��  ��  9 m     ��
�� 
TEXT7 o      ���� 0 thenotetitle theNoteTitle5 @A@ l  $ $��������  ��  ��  A BCB l  $ $��DE��  D * $ Get the tags from the selected note   E �FF H   G e t   t h e   t a g s   f r o m   t h e   s e l e c t e d   n o t eC G��G r   $ ,HIH l  $ *J����J n   $ *KLK 2  ( *��
�� 
EVtgL n   $ (MNM 4   % (��O
�� 
cobjO m   & '���� N o   $ %���� 0 selected_note  ��  ��  I o      ���� 0 thenotetags theNoteTags��  ! R      ��P�
�� .ascrerr ****      � ****P o      �~�~ 0 errmsg errMsg�  " k   4 >QQ RSR l  4 4�}TU�}  T D > Throw error and exit, if we can't select the note in Evernote   U �VV |   T h r o w   e r r o r   a n d   e x i t ,   i f   w e   c a n ' t   s e l e c t   t h e   n o t e   i n   E v e r n o t eS WXW I  4 ;�|Y�{
�| .sysodlogaskr        TEXTY m   4 7ZZ �[[ f E R R O R :   C o u l d n ' t   s e l e c t   n o t e   -   P l e a s e   o p e n   E v e r n o t e !�{  X \�z\ L   < >�y�y  �z   ]^] l  ? ?�x�w�v�x  �w  �v  ^ _`_ l  ? ?�uab�u  a / ) Test to see if Evernote is synchronising   b �cc R   T e s t   t o   s e e   i f   E v e r n o t e   i s   s y n c h r o n i s i n g` ded W   ? Ofgf l  K K�thi�t  h $  Wait until sync has completed   i �jj <   W a i t   u n t i l   s y n c   h a s   c o m p l e t e dg =  C Jklk 1   C H�s
�s 
EVscl m   H I�r
�r boovfalse mnm l  P P�q�p�o�q  �p  �o  n opo l  P P�nqr�n  q I C Force Evernote to Synchronise so we can get the resulting note URL   r �ss �   F o r c e   E v e r n o t e   t o   S y n c h r o n i s e   s o   w e   c a n   g e t   t h e   r e s u l t i n g   n o t e   U R Lp tut l  P P�mvw�m  v K E (The note URL is only assigned to the note once a sync has happened)   w �xx �   ( T h e   n o t e   U R L   i s   o n l y   a s s i g n e d   t o   t h e   n o t e   o n c e   a   s y n c   h a s   h a p p e n e d )u yzy I  P U�l�k�j
�l .EVRNsyncnull��� ��� null�k  �j  z {|{ l  V V�i�h�g�i  �h  �g  | }~} l  V V�f��f   : 4 Wait until there is a note URL assigned to the note   � ��� h   W a i t   u n t i l   t h e r e   i s   a   n o t e   U R L   a s s i g n e d   t o   t h e   n o t e~ ��� V   V m��� k   ^ h�� ��� l  ^ ^�e���e  � . ( Get the note URL from the selected note   � ��� P   G e t   t h e   n o t e   U R L   f r o m   t h e   s e l e c t e d   n o t e� ��d� r   ^ h��� l  ^ f��c�b� n   ^ f��� 1   b f�a
�a 
EV24� n   ^ b��� 4   _ b�`�
�` 
cobj� m   ` a�_�_ � o   ^ _�^�^ 0 selected_note  �c  �b  � o      �]�] 0 
thenoteurl 
theNoteURL�d  � =  Z ]��� o   Z [�\�\ 0 
thenoteurl 
theNoteURL� m   [ \�[
�[ 
msng� ��� l  n n�Z�Y�X�Z  �Y  �X  � ��� l  n n�W���W  � - ' if the property 'userTags' is set to 1   � ��� N   i f   t h e   p r o p e r t y   ' u s e r T a g s '   i s   s e t   t o   1� ��� Z   n ����V�U� l  n u��T�S� =   n u��� o   n s�R�R 0 usertags userTags� m   s t�Q�Q �T  �S  � k   x ��� ��� l  x x�P���P  � C = Ask the user which tags they would like to apply to the task   � ��� z   A s k   t h e   u s e r   w h i c h   t a g s   t h e y   w o u l d   l i k e   t o   a p p l y   t o   t h e   t a s k� ��� r   x ���� l  x ���O�N� I  x ��M��
�M .gtqpchltns    @   @ ns  � o   x }�L�L 0 taglist tagList� �K��
�K 
prmp� m   � ��� ��� 6 S e l e c t   t h e   t a g s   t o   i n c l u d e :� �J��I
�J 
mlsl� m   � ��H
�H boovtrue�I  �O  �N  � o      �G�G 0 thetags theTags� ��� l  � ��F�E�D�F  �E  �D  � ��� l  � ��C���C  � I C If any tags are seleceted the user, add them to the string of tags   � ��� �   I f   a n y   t a g s   a r e   s e l e c e t e d   t h e   u s e r ,   a d d   t h e m   t o   t h e   s t r i n g   o f   t a g s� ��B� Z   � ����A�@� >  � ���� o   � ��?�? 0 thetags theTags� m   � ��>
�> boovfals� X   � ���=�� r   � ���� b   � ���� b   � ���� o   � ��<�< 0 thecustomtags theCustomTags� m   � ��� ���  ,� o   � ��;�; 0 thetag theTag� o      �:�: 0 thecustomtags theCustomTags�= 0 thetag theTag� o   � ��9�9 0 thetags theTags�A  �@  �B  �V  �U  � ��� l  � ��8�7�6�8  �7  �6  � ��� l  � ��5���5  � - ' if the property 'noteTags' is set to 1   � ��� N   i f   t h e   p r o p e r t y   ' n o t e T a g s '   i s   s e t   t o   1� ��4� Z   ����3�2� l  � ���1�0� =   � ���� o   � ��/�/ 0 notetags noteTags� m   � ��.�. �1  �0  � k   � ��� ��� l  � ��-���-  � G A If any tasks present on the note, add them to the string of tags   � ��� �   I f   a n y   t a s k s   p r e s e n t   o n   t h e   n o t e ,   a d d   t h e m   t o   t h e   s t r i n g   o f   t a g s� ��,� Z   � ����+�*� >  � ���� o   � ��)�) 0 thenotetags theNoteTags� m   � ��(
�( boovfals� X   � ���'�� r   � ���� b   � ���� b   � ���� o   � ��&�& 0 thecustomtags theCustomTags� m   � ��� ���  ,� n   � ���� 1   � ��%
�% 
pnam� o   � ��$�$ 0 thetag theTag� o      �#�# 0 thecustomtags theCustomTags�' 0 thetag theTag� o   � ��"�" 0 thenotetags theNoteTags�+  �*  �,  �3  �2  �4   5    �!�� 
�! 
capp� m   
 �� ��� * c o m . e v e r n o t e . E v e r n o t e
�  kfrmID  ��  ��   ��� l     ����  �  �  � ��� l     ����  � - ' Format the task notes (add linefeeds)    � ��� N   F o r m a t   t h e   t a s k   n o t e s   ( a d d   l i n e f e e d s )  � ��� l ���� r  ��� b     b   o  �� 0 
thenoteurl 
theNoteURL o  �
� 
ret  1  �
� 
lnfd� o      �� 0 mynote myNote�  �  �  l     ����  �  �    l     �	�   $  Remove any &'s from the title   	 �

 <   R e m o v e   a n y   & ' s   f r o m   t h e   t i t l e  l !�� r  ! n  I  ��� 0 replace_chars    o  �� 0 thenotetitle theNoteTitle  m   �  & � m   �  a n d�  �    f   l     ��
 o      �	�	 0 mytask myTask�  �
  �  �    l     ����  �  �     l     �!"�  ! 4 . Create the tasks in 2Do (using the app's URL)   " �## \   C r e a t e   t h e   t a s k s   i n   2 D o   ( u s i n g   t h e   a p p ' s   U R L )  $%$ l "M&��& I "M�'�
� .sysoexecTEXT���     TEXT' b  "I()( b  "E*+* b  "C,-, b  "=./. b  "9010 b  "5232 b  "1454 b  "-676 b  ")898 m  "%:: �;; & o p e n   ' t w o d o : / / / a d d ?9 m  %(<< �== 
 t a s k =7 o  ),� �  0 mytask myTask5 m  -0>> �??  & f o r l i s t =3 m  14@@ �AA  & n o t e =1 o  58���� 0 mynote myNote/ m  9<BB �CC  & t a g s =- o  =B���� 0 
defaulttag 
defaultTag+ o  CD���� 0 thecustomtags theCustomTags) m  EHDD �EE  '�  �  �  % FGF l     ��������  ��  ��  G HIH l     ��JK��  J ; 5-----------------------------------------------------   K �LL j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -I MNM l     ��OP��  O ; 5             FUNCTION: Replace Chars               --   P �QQ j                           F U N C T I O N :   R e p l a c e   C h a r s                               - -N RSR l     ��TU��  T ; 5-----------------------------------------------------   U �VV j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -S W��W i    XYX I      ��Z���� 0 replace_chars  Z [\[ o      ���� 0 	this_text  \ ]^] o      ���� 0 search_string  ^ _��_ o      ���� 0 replacement_string  ��  ��  Y k      `` aba r     cdc l    e����e o     ���� 0 search_string  ��  ��  d n     fgf 1    ��
�� 
txdlg 1    ��
�� 
ascrb hih r    jkj n    	lml 2    	��
�� 
citmm o    ���� 0 	this_text  k l     n����n o      ���� 0 	item_list  ��  ��  i opo r    qrq l   s����s o    ���� 0 replacement_string  ��  ��  r n     tut 1    ��
�� 
txdlu 1    ��
�� 
ascrp vwv r    xyx c    z{z l   |����| o    ���� 0 	item_list  ��  ��  { m    ��
�� 
TEXTy o      ���� 0 	this_text  w }~} r    � m    �� ���  � n     ��� 1    ��
�� 
txdl� 1    ��
�� 
ascr~ ���� L     �� o    ���� 0 	this_text  ��  ��       ��� q���������  � �������������� 0 
defaulttag 
defaultTag�� 0 usertags userTags�� 0 notetags noteTags�� 0 taglist tagList�� 0 replace_chars  
�� .aevtoappnull  �   � ****�� �� � ����� �   � � � �� ��Y���������� 0 replace_chars  �� ����� �  �������� 0 	this_text  �� 0 search_string  �� 0 replacement_string  ��  � ���������� 0 	this_text  �� 0 search_string  �� 0 replacement_string  �� 0 	item_list  � ���������
�� 
ascr
�� 
txdl
�� 
citm
�� 
TEXT�� !���,FO��-E�O���,FO��&E�O���,FO�� �����������
�� .aevtoappnull  �   � ****� k    M��  ���  ��� �� ��� �� $����  ��  ��  � ������ 0 errmsg errMsg�� 0 thetag theTag� / ��������������������������������Z�������������������������������������:<>@BD���� 0 thecustomtags theCustomTags
�� 
msng�� 0 
thenoteurl 
theNoteURL
�� 
capp
�� kfrmID  
�� 
EV15�� 0 selected_note  
�� 
cobj
�� 
EVet
�� 
TEXT�� 0 thenotetitle theNoteTitle
�� 
EVtg�� 0 thenotetags theNoteTags�� 0 errmsg errMsg��  
�� .sysodlogaskr        TEXT
�� 
EVsc
�� .EVRNsyncnull��� ��� null
�� 
EV24
�� 
prmp
�� 
mlsl�� 
�� .gtqpchltns    @   @ ns  �� 0 thetags theTags
�� 
kocl
�� .corecnte****       ****
�� 
pnam
�� 
ret 
�� 
lnfd�� 0 mynote myNote�� 0 replace_chars  �� 0 mytask myTask
�� .sysoexecTEXT���     TEXT��N�E�O�E�O)���0 � *�,E�O��k/�,�&E�O��k/�-E�W X  a j OhO h*a ,f hY��O*j O h�� ��k/a ,E�[OY��Ob  k  Mb  a a a ea  E` O_ f ' !_ [a �l kh �a %�%E�[OY��Y hY hOb  k  3�f ) #�[a �l kh �a %�a  ,%E�[OY��Y hY hUO�_ !%_ "%E` #O)�a $a %m+ &E` 'Oa (a )%_ '%a *%a +%_ #%a ,%b   %�%a -%j . ascr  ��ޭ