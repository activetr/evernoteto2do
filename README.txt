========================
Evernote To 2Do - 1.0.2
========================

Author: James Gibbard (http://jgibbard.me.uk)
Website: http://jgibbard.me.uk/bitbucket/evernoteto2do

EvernoteTo2Do allows you to easily create tasks in 2Do from your notes in 
Evernote. You can import the Evernote tags you already have setup, and choose
from a list of custom tags that you can optionally set.

Installation:
--------------
The best way I found to install Applescripts into Evernote is to use them as
an OS X Service and assign them to a shortcut key.

1. Download the OS X Service DMG installer (EvernoteTo2Do - http://bit.ly/1bvFPAI)
2. Install the service on your computer.
3. Open 'System Preferences', go to Keyboard -> Shortcuts -> Services
4. Select the ‘EvernoteTo2Do’ service; Assign a shortcut key to the service.


Usage:
-------
1. Open Evernote
2. Select a note in the list
3. Press your chosen shortcut key.
4. Watch your selected note appear in 2Do as a task - and smile :) 